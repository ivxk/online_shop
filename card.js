function Product(name, category, price, valute, newOrUsed, img) {
    this.name = name;
    this.category = category;
    this.price = price;
    this.valute = valute;
    this.newOrUsed = newOrUsed;
    this.img = img;
}

const productArray = [];
let filteredArray = [];
        
function create() {
    const name = document.getElementById('name').value;
    const category = document.getElementById('category').selectedOptions[0].value;
    const price = parseInt(document.getElementById('price').value);
    const valute = document.getElementById('valute').selectedOptions[0].value;
    const newOrUsed = document.querySelector("input[name=new_used]:checked").value;
    const img = document.getElementById('img').value;
    // product object
    const product = new Product(name, category, price, valute, newOrUsed, img);
    // product object array
    productArray.push(product);

    filter();
    // displayCard(productArray);
}
let sortmi = 0;
let search = 0;
function sorting(){
    sortmi = 1;
    filter();
}
function resorting(){
    sortmi = 0;
    filter();
}
function displayCard(array) {
    const x = document.querySelector(".main");
    x.innerHTML = '';
    if(sortmi)
    {
        array.sort(function(a, b)
            { return a.price - b.price});
    }
    // console.log(array);
    array.forEach(product => {
        x.innerHTML += 
        `<div class="card" style="width: 15rem;">
            <img src=${product.img} class="card-img-top" alt="Product">
            <div class="card-body">
                <h5 class="card-title">${product.name}</h5>
                <p class="card-text">Category: <span style="font-weight: bolder">${product.category}</p>
                <p class="card-text">Price: <span style="font-weight: bolder">${product.price} ${product.valute}</p>
                <p class="card-text">Condition: <span style="font-weight: bolder">${product.newOrUsed}</p>
                <a href="#" class="btn btn-primary">More Information</a>
            </div>
        </div>`;
    });
}

function filter() {
    const filt = document.getElementById('filter').selectedOptions[0].value;
    const filt1 = document.getElementById('filter1').selectedOptions[0].value;
    if (filt === 'all' && filt1 === 'none')
        filteredArray = productArray;
    else if(filt === 'all')
    {
        filteredArray = productArray.filter(product => product.newOrUsed === filt1);
    }
    else if(filt1 === 'none')
    {    
        filteredArray = productArray.filter(product => product.category === filt);
    }
    else{
        filteredArray = productArray.filter(product => product.category === filt && product.newOrUsed === filt1);
    }
    if(search)
    {
        const searchName = document.getElementById('searchName').value;
        // console.log(searchName);
        filteredArray = filteredArray.filter(product => product.name == searchName);
    }
    displayCard(filteredArray);
}
function searchByName(){
    search = 1;
    filter();
}
function resetSearch(){
    search = 0;
    filter();
}
window.onbeforeunload = function() {
    return true;
};